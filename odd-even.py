# (C) Zvonova Irina
# You can use this software under the MIT License

# bot's bitcoin address 1PggCUWvEWKW1j6k5R9ndMfey3v5uR9JfN

import socks, socket
import random, time
import json
import sys
from hashlib import sha256

from ircbot import SingleServerIRCBot
from electrum import Wallet
from electrum import WalletStorage
from electrum import SimpleConfig
from electrum import Network
from electrum import Transaction
from electrum import bitcoin
from electrum import mnemonic

def sha256hex(data):
    return sha256(data).hexdigest()

socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", 9050)
socket.socket = socks.socksocket

config = SimpleConfig({'wallet_path': 'odd-even.wallet'})
storage = WalletStorage(config)
wallet = Wallet(storage)
if not storage.file_exists:
    wallet.init_seed(None)
    wallet.set_fee(10000)
    wallet.save_seed(password=None)
    wallet.create_accounts(password=None)
    wallet.save_accounts()
    print("Seed created. Public key is " +
            wallet.get_master_public_key())
    storage.write()
account = wallet.accounts.values()[0]

new_num = storage.get('odd-even-new-num', 0)
irc_num = storage.get('odd-even-irc-num', -1)
secret = sha256hex(wallet.seed + '_seed')
cat_token = sha256hex(secret + '_cat')
message_token = sha256hex(secret + '_message')
irc_server = storage.get('odd-even-irc-server', 'renko743grixe7ob.onion')
irc_channel = storage.get('odd-even-irc-channel', '#casino-royal-btc')
irc_port = storage.get('odd-even-irc-port', 6667)
prize_factor = storage.get('odd-even-prize-factor', 2000) # 1/1000

PRESERVED_ADDRESSES = 5
SIGNING_ADDRESS_NUM = 0

network = None
while not network or not network.interface.is_connected:
    print("Trying to connect...")
    network = Network(config)
    network.start(wait=True)
    time.sleep(1)
wallet.start_threads(network)
wallet.synchronize()
print("Connected!")
time.sleep(10) # to download all tx

def irc_send(texts, channel, nick, real, server, port):
    class IrcSender(SingleServerIRCBot):
        def on_welcome(self, c, e):
            c.join(channel)
            for num, text in texts:
                time.sleep(3)
                c.privmsg(channel, text)
                if type(num) == int:
                    storage.put('odd-even-irc-num', num)
                    storage.write()
            time.sleep(3)
            self.die() # exits the program
        def on_ctcp(self, c, e):
            pass
        def on_dccchat(self, c, e):
            pass
    print('IRC sending...')
    sender = IrcSender([[server, port]], nick, real)
    sender.start()

class Game(object):
    def __init__(self, num):
        self.num = num
        self.num0 = PRESERVED_ADDRESSES + num * 2
        self.num1 = PRESERVED_ADDRESSES + num * 2 + 1
        self.add0 = account.get_address(for_change=0, n=self.num0)
        self.add1 = account.get_address(for_change=0, n=self.num1)
        addrs = [self.add0, self.add1]
        addrs.sort()
        self.first, self.second = addrs
        h = sha256hex(self.add0 + cat_token + self.add1)
        addrs = [self.add0, self.add1]
        self.winner_num = int(h, 16) % 2
        self.winner_addr = addrs[self.winner_num]
        self.loser_num = 1 - self.winner_num
        self.loser_addr = addrs[self.loser_num]
        self.message_salt = sha256hex(self.add0 + message_token + self.add1)
        self.message_salt = self.message_salt[:32]
        self.cleartext = self.message_salt + ' ' + self.winner_addr
        self.sha = sha256hex(self.cleartext)
        while len(account.addresses) <= self.num1:
            account.create_new_address(for_change=False)
        storage.write()

    def challenge_text(self):
        return ('NEW GAME #%i. Pay to\x032 %s\x0f or'+\
               '\x032 %s\x0f right now. ' +\
               'SHA256 of "random_token winner_address" is %s') %\
               (self.num, self.first, self.second, self.sha)

    def winner_text(self):
        addrs = [self.add0, self.add1]
        addrs.sort()
        return ('Game #%i. Winner address is\x039 %s\x0f. '+\
               'SHA256 of "%s" is %s. Loser address is\x034 %s\x0f') %\
               (self.num, self.winner_addr,
                       self.cleartext, self.sha,
                       self.loser_addr)

def get_game(num):
    if not hasattr(get_game, 'dict'):
        get_game.dict = {}
    if num not in get_game.dict:
        get_game.dict[num] = Game(num)
    return get_game.dict[num]

def random_nick():
    beginning = random.choice(mnemonic.words)
    beginning = beginning[:len(beginning)/2]
    ending = random.choice(mnemonic.words)
    ending = ending[len(ending)/2:]
    return beginning + ending

texts = []
def add_text(num, text):
    texts.append((num, text))
    print(text)

signing_address = account.get_address(for_change=0,
        n=SIGNING_ADDRESS_NUM)
HELLO_TEXT_1 = 'Hello! This is bitcoin odd-even game. '+\
        'Payment to winner address is awarded via fold (in 2 games) '+\
        'with %i%% prize minus transaction fee. '+\
        'SHA256 of (random token & winner address) is provided '+\
        'to let everybody make sure I do not cheat :)'
HELLO_TEXT_2 = 'Number of new game, SHA256 and payment addresses '+\
        'are signed with my bitcoin '+\
        'address %s. Please check signature bellow! '+\
        'Do not pay to old addresses announced. '+\
        'Unconfirmed payments or payments to old addresses '+\
        'are not awarded.'
prize = float(prize_factor) / 10
add_text(None, HELLO_TEXT_1 % prize)
add_text(None, HELLO_TEXT_2 % signing_address)
disclose_nums = range(irc_num + 1, new_num - 1)
for num in disclose_nums:
    add_text(num, get_game(num).winner_text())
add_text(None, get_game(new_num).challenge_text())
signed_text = "%i %s %s %s" % \
    (new_num,
     get_game(new_num).sha,
     get_game(new_num).first,
     get_game(new_num).second)
text_sign = wallet.sign_message(signing_address,
        signed_text, password=None)
add_text(None, 'Bitcoin signature of "%s" is %s' %
        (signed_text, text_sign))

disclose_num = new_num - 2
winner_senders = {} # {'addr': satoshi_to_send_back}
if disclose_num >= 0:
    disclose_game = get_game(disclose_num)
    winner_addr = disclose_game.winner_addr
    history = network.synchronous_get([
            ('blockchain.address.get_history',[winner_addr])
        ])[0]
    for item in history:
        tx_hash = item['tx_hash']
        tx_height = item['height']
        if tx_height:
            # confirmed
            raw_tx = network.synchronous_get([
                    ('blockchain.transaction.get',
                        [tx_hash, tx_height])
                ])[0]
            tx = Transaction(raw_tx)
            sent_satoshi = 0
            for addr, satoshi in tx.outputs:
                if addr == winner_addr:
                    sent_satoshi += satoshi
            sent_satoshi *= prize_factor // 1000
            if sent_satoshi:
                sender = tx.inputs[0]['address']
                if sender.startswith('1'):
                    winner_senders[sender] = winner_senders.get(sender, 0) + \
                            sent_satoshi

all_satoshi = sum(winner_senders.values())
tx = None
if all_satoshi > 0:
    try:
        tx = wallet.make_unsigned_transaction(winner_senders.items(), fee=0)
    except:
        add_text(None, "Not enough funds for game #%i prize!" %\
                 disclose_num)
if tx:
    tx_size = 181 * len(tx.inputs) + 34 * len(tx.outputs) + 10
    fee = wallet.fee * int(round(tx_size/1024.))
    if fee == 0:
        fee = wallet.fee
    if all_satoshi < fee:
        add_text(None, ("Game #%i. "+\
                    "Award %i satoshis is less than fee %i satoshis.") %
                    (disclose_num, all_satoshi, fee))
    else:
        users_receive = all_satoshi - fee
        outputs = []
        for (addr, satoshi) in tx.outputs:
            if addr in winner_senders:
                # not our "change" address
                satoshi = satoshi * users_receive // all_satoshi
            outputs.append((addr, satoshi))
        tx.outputs = outputs
        sum_inputs = sum(inp['value'] for inp in tx.inputs)
        sum_outputs = sum(out[1] for out in tx.outputs)
        tx_fee = sum_inputs - sum_outputs
        assert tx_fee >= fee
        assert tx_fee <= fee * 2 # insurance
        input_info = json.loads(tx.as_dict()['input_info'])
        wallet.signrawtransaction(tx, input_info,
                private_keys=[], password=None)
        print(tx.raw)
        print(tx.hash())
        success, error_message = wallet.sendtx(tx)
        if not success:
            print('failed to broadcast transaction. Error: %s. Exit' %
                    error_message)
            sys.exit(1)
        add_text(None, ("Game #%i. "+\
                    "Sent\x039 %i satoshis to %i users\x0f. Congrats! "
                    "Transaction hash %s") %
                    (disclose_num, users_receive,
                     len(winner_senders), tx.hash()))

new_num += 1
storage.put('odd-even-new-num', new_num)
storage.write()

nick = random_nick()

irc_send(texts, irc_channel, nick, nick, irc_server, irc_port)

